/**
 * 基本测试周期执行
 * task0 执行周围200ms
 * task1 执行周围400ms
 */

#include "pt_os.h"
#include "stdio.h"


static int task0()
{
    PtOsDelay(200);
    printf("t0\n");
}


static int task1(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
    while (1) {
        printf("t1\n");
        PT_DELAY(400);
    }
    PT_END(pt);
}





void pt_demo00_init(void)
{
    PtOsTaskCreate(task0);
    PtOsTaskCreate(task1);
}