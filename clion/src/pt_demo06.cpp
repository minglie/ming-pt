
#include "Protothread.h"
#include "stdio.h"
#include "Arduino.h"

class LEDFlasher : public Protothread
{
    public:
        virtual bool Run();
};
bool LEDFlasher::Run()
{
    static long t1 = millis();
    PT_BEGIN();
    while (1)
    {

        printf("  %ld \n", millis()-t1);
        t1 = millis();
        PT_DELAY(100);
    }
    PT_END();
}



class LEDFlasher1 : public Protothread
{
public:
    virtual bool Run();
};
bool LEDFlasher1::Run()
{
    PT_BEGIN();
    while (1)
    {
      //  printf("2\n");
        PT_DELAY(200);
    }
    PT_END();
}






LEDFlasher ledFlasher;
LEDFlasher1 ledFlasher1;

#ifdef __cplusplus
extern "C" {
#endif

void pt_demo06_init(void)
{
    while (1){

        Protothread::OnTickAll();
        delay(10);
    }
}

#ifdef __cplusplus
}
#endif