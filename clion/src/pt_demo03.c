/**
 *  同步调用子协程并传参
 */

#include "pt_os.h"
#include "stdio.h"


static int task0(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
                while (1)
                {
                    //等待被其他任务调用
                    PT_YIELD_UNTIL_CALLED(pt);
                    printf("t0: receive %d\n", pt->inData);
                    //任务1运行2s
                    PT_DELAY(200);
                    //任务1返回88
                    pt->outData = 88;
                    PT_EXIT(pt);
                }
    PT_END(pt);
}



static int task1(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
    while (1)
    {
        //调用任务0,并传入参数33,并等待返回
        PT_CALL_TASK(pt, 0, 33);
        printf("t1:t0 return: %d\n", pt_os_task[0].outData);
        PT_DELAY(200);
    }
    PT_END(pt);
}


void pt_demo03_init(void)
{
     PtOsTaskCreate(task0);
     PtOsTaskCreate(task1);
}