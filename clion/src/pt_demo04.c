/**
 * 协程间信号量通讯
 * task0执行3次后,task1与task0交替执行
 */

#include "pt_os.h"
#include "stdio.h"

static struct pt_sem mutex;

static int task0(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
                while (1)
                {
                    //收信号量
                    PT_SEM_WAIT(pt, &mutex);
                    printf("task0 run1 \n");
                }
    PT_END(pt);
}


static int task1(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
                //先让 task0 先运行3次
                PT_SEM_INIT(&mutex, 3);
                while (1)
                {
                    PT_DELAY(1000);
                    //发信号量
                    PT_SEM_SIGNAL(pt, &mutex);
                    printf("task1 run \n");
                }
    PT_END(pt);
}


void pt_demo04_init(void)
{
     PtOsTaskCreate(task0);
     PtOsTaskCreate(task1);
}