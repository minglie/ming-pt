#include "Arduino.h"
#include "pt_os.h"
#include "pt_demo.h"




void setup() {
    pinMode(2, OUTPUT);
    pinMode(26, OUTPUT);
    Serial.begin(115200);
    pt_demo_main();
}

void loop() {
    delay(1);
    PtOsTimeTick();
}

#if IS_VS_ENV
int main() {
    Main();

    return 0;
}
#endif