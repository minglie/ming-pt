
#include "Protothread.h"
#include "stdio.h"
#include "Arduino.h"

class M_LEDFlasher : public Protothread
{
    public:
        virtual bool Run();
};
bool M_LEDFlasher::Run()
{
    static long t1 = millis();
    PtOsDelay(100);
    printf("  %ld \n", millis() - t1);
    t1 = millis();
    return 0;
}




M_LEDFlasher ledFlasher;


#ifdef __cplusplus
extern "C" {
#endif

void pt_demo07_init(void)
{
    while (1){

        Protothread::OnTickAll();
        delay(10);
    }
}

#ifdef __cplusplus
}
#endif