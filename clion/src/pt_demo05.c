/*
 * 不使用PT宏,纯函数生产消费轮询写法
 * task1每300ms生产一条数据,task0每100ms消费一条数据
 */
#include "pt_os.h"
#include "stdio.h"

int task0()
{
	unsigned long v = PtPopInData();
	if (v) {
        printf("t0:consume %d \n", 3);
	}
	PtOsDelay(500);
}



int task1()
{
	static int inx = 1;
	int r = PtPushIndata(0, inx);
	if (r == 0) {
		inx++;
	}
	printf("t1:product %d \n", r);
	PtOsDelay(300);
}


void pt_demo05_init(void)
{
	PtOsTaskCreate(task0);
	PtOsTaskCreate(task1);
}