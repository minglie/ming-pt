/**
 *  两个协程交替执行
 * 
 * task0   1_2_3___________1_2_3
 * task1   _1_2_3___________1_2_3
 */

#include "pt_os.h"
#include "stdio.h"

static int t1;
static int t2;

static int task0(PT_OS_TCB_TypeDef* pt)
{
	PT_BEGIN(pt);
	while (1) {
		printf("t0 %d \n", 1);
		PT_YIELD(pt);
		printf("t0 %d \n", 2);
		PT_YIELD(pt);
		printf("t0 %d \n", 3);
		PT_DELAY(100);
	}
	PT_END(pt);
}



static int task1(PT_OS_TCB_TypeDef* pt)
{
	static int inx = 1;
	PT_BEGIN(pt);
	while (1) {
		printf("t1 %d \n", 1);
		PT_YIELD(pt);
		printf("t1 %d \n", 2);
		PT_YIELD(pt);
		printf("t1 %d \n", 3);
		PT_DELAY(100);

	}
	PT_END(pt);
}


void pt_demo01_init(void)
{
    t1 = PtOsTaskCreate(task0);
    t1 = PtOsTaskCreate(task1);
}