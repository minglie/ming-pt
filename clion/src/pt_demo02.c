/**
 *  等待条件 PT_WAIT_UNTIL或PT_WAIT_WHILE
 * task0 执行2次,task1执行1次
 */

#include "pt_os.h"
#include "stdio.h"

static int t1;
static int t2;
static int case_a = 0;

static int task0(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
    while (1)
    {
        PT_DELAY(100);
        printf("t0 \n");
        case_a++;
    }
    PT_END(pt);
}



static int task1(PT_OS_TCB_TypeDef* pt)
{
    PT_BEGIN(pt);
    while (1)
    {
        PT_WAIT_UNTIL(pt, case_a == 2);
        case_a = 0;
        printf("t1 \n");
    }
    PT_END(pt);
}


void pt_demo02_init(void)
{
    t1 = PtOsTaskCreate(task0);
    t1 = PtOsTaskCreate(task1);
}