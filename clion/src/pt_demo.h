#ifndef _pt_demo_H
#define _pt_demo_H

#ifdef __cplusplus
extern "C" {
#endif

void pt_demo00_init(void);
void pt_demo01_init(void);
void pt_demo02_init(void);
void pt_demo03_init(void);
void pt_demo04_init(void);
void pt_demo05_init(void);
void pt_demo06_init(void);
void pt_demo07_init(void);

void pt_demo_main() {
	pt_demo07_init();
}

#ifdef __cplusplus
}
#endif




#endif


