#ifndef __PT_OS_H__
#define __PT_OS_H__


#define config_max_tasks 3
typedef unsigned short lc_t;
#define LC_INIT(s) s = 0;
#define LC_RESUME(s) switch(s) { case 0:
#define LC_SET(s) s = __LINE__; case __LINE__:
#define LC_END(s) }
#define PT_WAITING 0
#define PT_EXITED  1
#define PT_ENDED   2
#define PT_YIELDED 3
//初始化
#define PT_INIT(pt)   LC_INIT((pt)->lc)
//声明和定义
#define PT_THREAD(name_args) char name_args
#define PT_BEGIN(pt) { char PT_YIELD_FLAG = 1; LC_RESUME((pt)->lc)
#define PT_END(pt) LC_END((pt)->lc); PT_YIELD_FLAG = 0; \
                   PT_INIT(pt); return PT_ENDED; }
//阻塞等待
#define PT_WAIT_UNTIL(pt, condition)	        \
  do {						\
    LC_SET((pt)->lc);				\
    if(!(condition)) {				\
      return PT_WAITING;			\
    }						\
  } while(0)
#define PT_WAIT_WHILE(pt, cond)  PT_WAIT_UNTIL((pt), !(cond))
#define PT_WAIT_THREAD(pt, thread) PT_WAIT_WHILE((pt), PT_SCHEDULE(thread))
#define PT_SPAWN(pt, child, thread)		\
  do {						\
    PT_INIT((child));				\
    PT_WAIT_THREAD((pt), (thread));		\
  } while(0)

#define PT_RESTART(pt)				\
  do {						\
    PT_INIT(pt);				\
    return PT_WAITING;			\
  } while(0)

#define PT_EXIT(pt)				\
  do {						\
    PT_INIT(pt);				\
    pt->callFunState=0;        \
    return PT_EXITED;			\
  } while(0)
#define PT_SCHEDULE(f) ((f) == PT_WAITING)
//让位
#define PT_YIELD(pt)				\
  do {						\
    PT_YIELD_FLAG = 0;				\
    LC_SET((pt)->lc);				\
    if(PT_YIELD_FLAG == 0) {			\
      return PT_YIELDED;			\
    }						\
  } while(0)

//等待条件
#define PT_YIELD_UNTIL(pt, cond)		\
  do {						\
    PT_YIELD_FLAG = 0;				\
    LC_SET((pt)->lc);				\
    if((PT_YIELD_FLAG == 0) || !(cond)) {	\
      return PT_YIELDED;			\
    }						\
  } while(0)

//等待被调用
#define PT_YIELD_UNTIL_CALLED(pt) PT_YIELD_UNTIL(pt, pt->callFunState== 1)
//调用其他协程
#define PT_CALL_TASK(pt, childId, params)		\
  do {						\
     pt_os_task[childId].callFunState = 1;				\
     pt_os_task[childId].inData=params;			\
     PT_YIELD_UNTIL(pt, pt_os_task[childId].callFunState==0);		\
  } while(0)


struct pt_sem {
    unsigned int count;
};
//信号量
#define PT_SEM_INIT(s, c) (s)->count = c
#define PT_SEM_WAIT(pt, s)	\
  do {						\
    PT_WAIT_UNTIL(pt, (s)->count > 0);		\
    --(s)->count;				\
  } while(0)
#define PT_SEM_SIGNAL(pt, s) ++(s)->count

/*
 * pt任务中的延时
 */
#define PT_DELAY(v)				\
  do {						\
    pt->delay = v;			\
    PT_WAIT_UNTIL(pt, pt->delay == 0);		\
  } while(0)

#define  WHILE(a) 
typedef struct {
    struct struct_tcb
    {
        unsigned rdy : 1;     //就绪
        unsigned enable : 1;  //启用禁用任务
    }one;
    lc_t lc;  //保存上次执行到的行号
    lc_t delay;//延时计数
    unsigned char step; //状态机步骤
    unsigned long inData;//输入
    unsigned long outData;//输出
    unsigned char callFunState;//函数运行状态0 未开始 1开始运行
    int  (*task)();
}PT_OS_TCB_TypeDef;

#ifdef __cplusplus 
extern "C"
{
#endif

    void  PtOsTimeTick();
    
    /*
     *   创建协程任务
     */
    int   PtOsTaskCreate(int (*task)(PT_OS_TCB_TypeDef* pt));
    
    /*
     *  非pt任务中的延时
     */
    void  PtOsDelay(unsigned short timeTick);
    void  PtOsDelayResume(int taskHandle);

    /**
     * 协程间交换数据
     */
    unsigned long PtGetInData();
    void PtSetInData(int taskHandle,unsigned long inData);
    unsigned long PtPopInData();
    int PtPushIndata(int taskHandle, unsigned long inData);
    unsigned long PtGetOutData(int taskHandle);
    void PtSetOutData(unsigned long outData);
    unsigned long PtPopOutData(int taskHandle);
    int PtPushOutData(unsigned long outData);
    /*
     * 任务列表
     */
    extern  PT_OS_TCB_TypeDef  pt_os_task[config_max_tasks];
    /**
     * 当前协程下标
     */
    extern unsigned char OSTCBCur;
#ifdef __cplusplus 
}
#endif

#endif

