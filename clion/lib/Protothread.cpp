#include "Protothread.h"

int  Protothread::M_npt = 0;
Protothread * Protothread::M_pts[10];

Protothread::Protothread(): _ptLine(0){
    m_delay = 0;
    m_id=Protothread::M_npt++;
    M_pts[m_id]= this;
    indata=nullptr;
    outdata=nullptr;
    state=0;
}

void Protothread::PtOsDelay(unsigned long tick) {
   m_delay=tick;
}

void Protothread::PtOsDelayResume() {
    m_delay=0;
}

void Protothread::OnTick(){
    if (m_delay > 0)
        m_delay--;
    if(m_delay==0){
        Run();
     }
}

void Protothread::OnTickAll() {
    for (int i=0;i<M_npt;i++){
        M_pts[i]->OnTick();
    }
}