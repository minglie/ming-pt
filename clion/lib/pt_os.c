#include "pt_os.h"

 
unsigned char OSTCBCur;
unsigned char  os_task_index = 0;
PT_OS_TCB_TypeDef pt_os_task[config_max_tasks];
int PtOsTaskCreate(int (*task)(PT_OS_TCB_TypeDef*))
{
	int taskInx = os_task_index++;
	pt_os_task[taskInx].delay = 0;
	pt_os_task[taskInx].one.enable = 1;
	pt_os_task[taskInx].inData = 0;
	pt_os_task[taskInx].outData = 0;
	pt_os_task[taskInx].task = task;
	pt_os_task[taskInx].callFunState = 0;
	PT_INIT(&pt_os_task[taskInx]);
	return taskInx;
}

void PtOsTimeTick()
{
	for (int i = 0; i < os_task_index; i++) {
		OSTCBCur = i;
		PT_OS_TCB_TypeDef* pt = &pt_os_task[i];
		if (pt->delay == 0) {
			pt->one.rdy = 1;
		}
		else {
			pt->delay = pt->delay - 1;
		}
		if (pt->one.rdy && pt->one.enable) {
			pt->task(pt);
		}
	}
}

void PtOsDelay(unsigned short timeTick) {
	pt_os_task[OSTCBCur].one.rdy = 0;
	pt_os_task[OSTCBCur].delay = timeTick - 1;
}

void PtOsDelayResume(int taskHandle){
    pt_os_task[taskHandle].delay = 0;
}


unsigned long PtGetIndata() {
	return pt_os_task[OSTCBCur].inData;
}

void PtSetInData(int taskHandle, unsigned long inData) {
	pt_os_task[taskHandle].inData = inData;
}

unsigned long PtPopInData() {
	unsigned long v = pt_os_task[OSTCBCur].inData;
	pt_os_task[OSTCBCur].inData = 0;
	return v;
}
int PtPushIndata(int taskHandle, unsigned long inData) {
	if (pt_os_task[taskHandle].inData == 0) {
		pt_os_task[taskHandle].inData = inData;
		return 0;
	}
	return 1;
}

unsigned long PtGetOutData(int taskHandle) {
	return pt_os_task[taskHandle].outData;
}

void PtSetOutData(unsigned long outData) {
	pt_os_task[OSTCBCur].outData = outData;
}

unsigned long PtPopOutData(int taskHandle) {
	unsigned long v = pt_os_task[taskHandle].outData;
	pt_os_task[taskHandle].outData = 0;
	return v;
}

int PtPushOutData(unsigned long outData) {
	if (pt_os_task[OSTCBCur].outData == 0) {
		pt_os_task[OSTCBCur].outData = outData;
		return 0;
	}
	return 1;
}

