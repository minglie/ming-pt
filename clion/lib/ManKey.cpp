#include "ManKey.h"
#include "stddef.h"


ManKeyGetKeyboardMask* ManKey::digitalRead = NULL;
ManKeyOutEvent* ManKey::onEvent = NULL;
int  ManKey::M_nDevices = 0;
ManKey * ManKey::M_devices[10];

static ManKeyEventCode buildManKeyEventCode(uint8_t evtCode ,uint8_t inx){
    ManKeyEventCode manKeyEventCode;
    manKeyEventCode.one.evtCode=evtCode;
    manKeyEventCode.one.inx=inx;
    manKeyEventCode.one.arg=0;
    return manKeyEventCode;
}

ManKey::ManKey(uint8_t inx){
    m_inx=inx;
    M_devices[M_nDevices++]= this;
}

void ManKey::tick(uint32_t ms)
{
    uint16_t keyPressState = digitalRead(m_inx);
    switch (m_state)
    {
        case ManKeyState::STT_IDLE:
        default:
            if (keyPressState == 1)
            { 
                m_downMs = ms;
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_DOWN, m_inx));
                m_state = ManKeyState::STT_WAITING_CLICK_UP;
            }
            break;
        case ManKeyState::STT_WAITING_CLICK_UP:
            if (ms - m_downMs > 300)
            {
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_PRESSING, m_inx));
                m_pressingCount = 1;
                m_state = ManKeyState::STT_LONG_PRESSING;
            }
            else if (keyPressState == 0)
            { 
                m_state = ManKeyState::STT_WAITING_DBL_CLICK_DOWN;
            }
            break;
        case ManKeyState::STT_WAITING_DBL_CLICK_DOWN:
            if (ms - m_downMs > 400)
            {
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_CLICK, m_inx));
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_UP, m_inx));
                m_state = ManKeyState::STT_IDLE;
            }
            else if (keyPressState == 1)
            {
                m_state = ManKeyState::STT_WAITING_DBL_CLICK_UP; // 遭遇再次按下，判断为DBL_CLICK，后续DBL_CLICK_UP
            }
            break;
        case ManKeyState::STT_WAITING_DBL_CLICK_UP:
            if (keyPressState == 0)
            { 
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_DBL_CLICK, m_inx));
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_UP, m_inx));
                m_state = ManKeyState::STT_IDLE;
            }
            break;
        case ManKeyState::STT_LONG_PRESSING:
            if (keyPressState == 0)
            { 
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_LONG_CLICK, m_inx));
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_UP, m_inx));
                m_state = ManKeyState::STT_IDLE;
            }
            else if (ms - m_downMs > 300 + m_pressingCount * 100)
            {
                onEvent(ms, buildManKeyEventCode(MAN_KEY_EVT_PRESSING, m_inx));
                m_pressingCount++;
            }
            break;
    }
}

void ManKey::tickAll(uint32_t ms) {
    for (int i=0;i<M_nDevices;i++){
        M_devices[i]->tick(ms);
    }
}