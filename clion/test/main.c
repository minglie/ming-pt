#include "../lib/pt_os.h"
int task0(PT_OS_TCB_TypeDef* pt)
{
    static int a=0;
    PT_BEGIN(pt);
    while(1) {
        a=1;
        PT_DELAY(100);
        a=0;
        PT_DELAY(100);
    }
    PT_END(pt);
}
