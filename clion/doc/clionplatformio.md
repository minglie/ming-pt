# platformio 安装

## platformio 需要安装的东西

P1:  python3  (用来下载东西与执行脚本)

P2:  platformio  for Clion (clion的platformio 插件)

P3:  platformio Core（platformio 本体，安装 D:\soft\Anaconda3\envs\platformio\Scripts）

P4: .platformio 与单片机型号相关的工具链与库(第一次用该芯片时安装),安装在   C:\Users\minglie\.platformio

## 安装 platformio Core
```md
# 虚拟环境列表
conda env list

# 创建虚拟环境
conda create -n  platformio  python==3.11.0

# 切换到虚拟环境
activate platformio

# 虚拟环境查看镜像源
pip config list

# 设置虚拟环境镜像源
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple

# 安装
(platformio) C:\Users\minglie\Downloads>pip install -U platformio  -i https://pypi.tuna.tsinghua.edu.cn/simple
```

# 配置platformio Core
![](img01.png)

# platformio.ini的写法
## 配置 stlink
```ini
; PlatformIO Project Configuration File
;
;   Build options: build flags, source filter
;   Upload options: custom upload port, speed and extra flags
;   Library options: dependencies, extra library storages
;   Advanced options: extra scripting
;
; Please visit documentation for the other options and examples
; https://docs.platformio.org/page/projectconf.html

[env:blackpill_f103c8]
platform = ststm32
board = blackpill_f103c8
framework = arduino

upload_protocol = stlink
upload_flags =
    -c set CPUTAPID 0x2ba01477
    -c set WORKAREASIZE 0x8000
```

## platformio.ini  安装库的写法
```ini
[env:your_environment]
platform = ...
board = ...
framework = ...

lib_deps =
    library_name1
    library_name2@1.0.0
    username/library_name3@^2.1.0

```
## platformio.ini  esp32

```ini
[platformio]
src_dir = ./

[env:pico32]
platform = espressif32
board = pico32
framework = arduino

monitor_speed = 115200
monitor_port =COM1
upload_port = COM1

lib_deps =Encoder

build_flags =
    -Ilib
    -Ilib/lettershell/inc
    -Isrc
    -Isrc/app
    -Isrc/bsp

build_src_filter = +<src/> +<src/bsp/>   +<lib/> -<lib/modman/>


```