"""
  这是使用 状态机+标志位 实现的蜂鸣器驱动,  滴滴---滴滴  (micropython)
"""
class ManActiveBuzzer():
    # setPort控制蜂鸣器引脚电平
    def __init__(self, setPort):
        self.sound_state = 0
        self.sound_delay = 0
        #响计数
        self.sound_ture=0
        #不响计数
        self.sound_false=0
        self.setPort=setPort
        self.alarmValue=0

    #控制叫声模式0:不叫 >0:单周期叫的次数
    def setState(self,sound_state):
        self.sound_state = sound_state

    #读端口
    @property
    def ALARM(self):
        return self.alarmValue
    #写端口
    @ALARM.setter
    def ALARM(self, value):
        self.alarmValue=value;
        self.setPort(value)

    #tick=4ms
    def onTick(self,ms):
        if self.sound_state>0:
            if self.sound_delay == 0:
                self.sound_delay = 40;
                if self.sound_false == 0:
                    self.sound_ture = self.sound_state;
                    #不响的稍微久一点
                    self.sound_false = self.sound_state+2;
                    self.ALARM = 1;
                #响
                elif  self.sound_ture>0:
                    if self.ALARM:
                        self.ALARM = 0;
                        self.sound_ture-=1;
                    else:
                        self.ALARM = 1;
                #不响
                else:
                    self.sound_false-=1;
            else:
                self.sound_delay-=1;
        else:
            self.ALARM = 0
