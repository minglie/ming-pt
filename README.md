# PT协程

原作者的[pt协程](https://dunkels.com/adam/pt/index.html)是一些宏组成的头文件,

本文的 [pt_os](clion/lib/readme.md)删减了一些宏, 并加了些方法

只有两个文件 pt_os.h 与 pt_os.c


[c版](clion/lib/pt_os.h)

[c++版](clion/lib/Protothread.h)

[纯函数版](vs/lib/ptos_cpp/pt_os.h)



## 使用场景

P1: 无RTOS环境的单片机中的多任务(无栈变量)

P2: [模拟低速时序](https://github.com/minglie/minglie_mcu_single_line_51_proteus/tree/master)，[状态机+标志位](clion/doc/ManActiveBuzzer.py)也能实现,但不优雅,不易看懂

P3: 非阻塞的同步调用。

## 缺点
P1: 不能使用局部变量

P2: 编译器支持的不够好

P3: 使用太多PT宏,宏替换后的代码不合法,会让代码难理解

## 文件目录

对应两个测试环境,其中vs引用了clion项目的文件

[vs](vs/README.md) 

[clion](clion/README.md)


## 使用步骤

P1: 用 [PtOsTaskCreate](clion/lib/pt_os.h)创建多个协程

P2: 周期调用 [PtOsTimeTick](clion/lib/pt_os.h) 运行协程

# PT协程的原理
下面代码会不断的打印BABA,但第一次打印的是B
```c
#include "stdio.h"
int main(void)
{
	int a = 11;
	switch (a)
	{ 
	    case 0:;
			while (1)
			{
				printf("A");
				case 11:;
				printf("B");
			}
	}
}
```
### 查看预处理文件,了解原理

```bash
gcc -E main.c -o main.i
```
```c
#include "../lib/pt_os.h"
int task0(PT_OS_TCB_TypeDef* pt)
{
    static int a=0;
    PT_BEGIN(pt);
    while(1) {
        a=1;
        PT_DELAY(100);
        a=0;
        PT_DELAY(100);
    }
    PT_END(pt);
}

```
### 预处理后的文件
```c
int task0(PT_OS_TCB_TypeDef* pt)
{
    static int a=0;
    {   char PT_YIELD_FLAG = 1; 
        switch((pt)->lc) { case 0:;
            while(1) {
                a=1;
                do { pt->delay = 100; do { 
                    (pt)->lc = 8; 
                    case 8:; 
                    if(!(pt->delay == 0)) { return 0; } } while(0); } while(0);
                a=0;
                do { 
                    pt->delay = 100; 
                    do { 
                        (pt)->lc = 10; 
                        case 10:; 
                        if(!(pt->delay == 0)) { return 0; } } while(0); } while(0);
            }
    }; PT_YIELD_FLAG = 0; (pt)->lc = 0;; return 2; };
}
```

# PT协程例子

[0.基本测试周期执行](clion/src/pt_demo00.c)

[1.两个协程交替执行](clion/src/pt_demo01.c)

[2.等待条件 PT_WAIT_UNTIL或PT_WAIT_WHILE](clion/src/pt_demo02.c)

[3.同步调用子协程并传参](clion/src/pt_demo03.c)

[4.协程间信号量通讯](clion/src/pt_demo04.c)

[5.不使用PT宏,纯函数生产消费轮询写法](clion/src/pt_demo05.c)

[6.c++版的pt协程](clion/src/pt_demo06.cpp)

[7.c++版的pt协程纯函数](clion/src/pt_demo07.cpp)



