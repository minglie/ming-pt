#ifndef __strtokmethod_h__
#define __strtokmethod_h__
#define  _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "stdarg.h"
#include "string.h"

typedef struct
{
	  int argc;
      char buf[3000];
      char method[20];
      char* argv[8];
} StrtokMethodTypeDef;


#ifdef __cplusplus
extern "C" {
#endif

    StrtokMethodTypeDef * strtokParseMethodStr(char* str);
    /**
     * hex字符串转字节数组
     **/
    int hexStringToByteArray(const char* hexStr, unsigned char* bs);
    /**
     * 去除串中的回车换行
     **/
    void str_clean_rn(const char* str);
    int str_to_int(const char* str);

#ifdef __cplusplus
}
#endif





#endif 
