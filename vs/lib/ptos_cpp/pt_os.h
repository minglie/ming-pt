#ifndef __PT_OS_H__
#define __PT_OS_H__

#define config_max_tasks 3


typedef unsigned short lc_t;
#define PT_INIT(pt)  
#define  WHILE(a) 
typedef struct {
    struct struct_tcb
    {
        unsigned rdy : 1;     //就绪
        unsigned enable : 1;  //启用禁用任务
    }one;
    lc_t lc;  //保存上次执行到的行号
    lc_t delay;//延时计数
    unsigned char step; //状态机步骤
    unsigned long inData;//输入
    unsigned long outData;//输出
    unsigned char callFunState;//函数运行状态0 未开始 1开始运行
    void  (*task)();
}PT_OS_TCB_TypeDef;

#ifdef __cplusplus 
extern "C"
{
#endif
    void  PtOsTimeTick();
    /*
     *   创建协程任务
     */
    int   PtOsTaskCreate(void (*task)());
    
    /*
     *  非pt任务中的延时
     */
    void  PtOsDelay(unsigned short timeTick);

    /**
     * 协程间交换数据
     */
    unsigned long PtGetInData();
    void PtSetInData(int taskHandle,unsigned long inData);
    unsigned long PtPopInData();
    int PtPushIndata(int taskHandle, unsigned long inData);
    unsigned long PtGetOutData(int taskHandle);
    void PtSetOutData(unsigned long outData);
    unsigned long PtPopOutData(int taskHandle);
    int PtPushOutData(unsigned long outData);
    /*
     * 任务列表
     */
    extern  PT_OS_TCB_TypeDef  pt_os_task[config_max_tasks];
    /**
     * 当前协程下标
     */
    extern unsigned char OSTCBCur;
#ifdef __cplusplus 
}
#endif

#endif

