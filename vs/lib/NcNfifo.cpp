#include "NcNFifo.h"
#include <string.h>
#include <stdio.h>


NcNfifo::NcNfifo(uint16_t capacity)
{
	m_capacity = capacity;
	m_buffer = new uint8_t[m_capacity];
	m_iPush = &m_loc[0];
	m_iPop = m_iPush;
	m_iEnd = &m_loc[NC_NFIFO_MAX_PACK_NUM - 1];
	m_iPush->startPtr = &m_buffer[0];
	m_len = 0;
	m_packCount = 0;
}

NcNfifo::~NcNfifo() {
	delete m_buffer;
}



bool NcNfifo::Push(uint8_t* buf, uint16_t len) {
	if (len == 0||len>NC_FIFO_MAX_PACK_LEN||m_packCount>=NC_NFIFO_MAX_PACK_NUM) {
		return false;
	}
	if (buf!=NULL) {
		memcpy(m_iPush->startPtr, buf, len);
	}
	m_len += len;	
	m_iPush->endPtr = &m_buffer[m_len];   		                 
	m_iPush++;                                                                             
	if (m_iPush > m_iEnd) {
		m_iPush = &m_loc[0];
	}  
	//剩余容量够一包数据
	if (m_capacity - m_len >= m_maxpack_len) {
		m_iPush->startPtr = &m_buffer[m_len];                    
	}
	else {                                                                                                           
		m_len = 0;                                                                         
		m_iPush->startPtr = &m_buffer[0];                                                       	
	}
	if(m_packCount<NC_NFIFO_MAX_PACK_NUM){
	  m_packCount++;
	}
	return true;
}


uint8_t* NcNfifo::Read(uint16_t* readLen) {

	if (m_iPop != m_iPush) {
		*readLen = m_iPop->endPtr - m_iPop->startPtr;
		return m_iPop->startPtr;
	}
	*readLen=0;
	return NULL;
}

void NcNfifo::ReadEnd() {

	//为空
	if (m_iPop == m_iPush) {
		m_packCount = 0;
		return;
	}
	m_iPop++;
	if (m_iPop > m_iEnd) {
		m_iPop = &m_loc[0];
	}
	if(m_packCount>0){
		m_packCount--;
	}
}