/**--------------无拷贝fifo----------**/

#ifndef __NcNFifo_h__
#define __NcNFifo_h__


#include <stdint.h>
#include <stdbool.h>
//包数量
#define NC_NFIFO_MAX_PACK_NUM 3
//单包最大长度
#define NC_FIFO_MAX_PACK_LEN 2048

typedef struct {
	uint8_t* startPtr;
	uint8_t* endPtr;
}NcNfifoLoc_TypeDef;


class NcNfifo
{
	protected:
		uint16_t   m_len;
		NcNfifoLoc_TypeDef  m_loc[NC_NFIFO_MAX_PACK_NUM]; //存放每个包在m_buffer在的位置                 
		NcNfifoLoc_TypeDef* m_iPush;         //写的位置               
		NcNfifoLoc_TypeDef* m_iPop;          //读的位置            
		NcNfifoLoc_TypeDef* m_iEnd;          //位置的尾部           
		uint8_t*  m_buffer;       //
		uint16_t  m_capacity;     //总容量
		uint16_t  m_maxpack_len; //单包最大字节数
		uint16_t  m_packCount;//当前包数量
	public:
		NcNfifo(uint16_t capacity);
		~NcNfifo();
		bool     Push(uint8_t* buf, uint16_t len);
		uint8_t* Read(uint16_t* readLen);
		void     ReadEnd();
	private:

};



                     
#endif 

