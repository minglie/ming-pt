#include "strtokmethod.h"
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>

int str_to_int(const char* str) {
    char* endptr;  // 用于存储不被转换的部分
    int base = 10;  // 默认解析为十进制
    if (str[0] == '0') {
        if (str[1] == 'x' || str[1] == 'X') {
            base = 16;  // 解析为十六进制
            str += 2;  // 跳过 0x 或 0X 前缀
        } else {
            base = 8;  // 解析为八进制
        }
    }
    long result = strtol(str, &endptr, base);  // 调用 strtol 函数进行转换
    if (*endptr != '\0') {
        // 如果有剩余字符未被转换，说明转换失败，返回 0（或者其他错误处理方式）
        return 0;
    }
    if (result > INT_MAX || result < INT_MIN) {
        // 如果转换后的结果超过 int 范围，返回 0（或者其他错误处理方式）
        return 0;
    }
    return (int)result;  // 正常转换完成，返回转换结果
}

void str_clean_rn(char* str) {
    char* q = str;
    while (*q) {
        if (*q=='\r') {
            *q = 0;
            break;
        }
        q++;
    }
}


int hexStringToByteArray(const char* hexStr, unsigned char* bs) {
    char token[3];
    int i = 0;
    long int byteValue;
    const int byteArrayLen = (strlen(hexStr) + 1) / 3;
    while (*hexStr && i < byteArrayLen) {
        if (isspace(*hexStr)) {
            hexStr++;
            continue;
        }

        if (!isxdigit(*hexStr)) {
            return i;
        }
        token[0] = *hexStr++;
        token[1] = *hexStr++;
        token[2] = '\0'; 
        byteValue = strtol(token, NULL, 16); 
        bs[i] = (unsigned char)byteValue;
        i++;
    }

    return i;
}
StrtokMethodTypeDef * strtokParseMethodStr(char *str)
{
    static StrtokMethodTypeDef srtd;
    srtd.argv[0] = srtd.buf;
    const char s[4] = "(,)";
    char* token;
    token = strtok(str, s);
    int i = 0;
    while (token != NULL)
    {
        if (i == 0)
        {
            sprintf(srtd.method, "%s", token);
            str_clean_rn(srtd.method);
            sprintf(srtd.argv[0], "%s", token);
            srtd.argv[1] = &srtd.buf[strlen(srtd.argv[0])+1];
        }
        else if (i < 8) {
            sprintf(srtd.argv[i], "%s", token);
            srtd.argv[i+1] = srtd.argv[i] + strlen(srtd.argv[i]) + 1;
        }
        token = strtok(NULL, s);
        i++;
        srtd.argc = i;
    }
    return &srtd;

}
