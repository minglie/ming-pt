#pragma once
#pragma warning(disable : 4996)
#pragma comment( lib,"winmm.lib" )
#include "windows.h"
#include "stdint.h"
#include<iostream>
#include <chrono>
#include <string>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <Mmsystem.h>
using namespace std;


class M
{
public:
	

	static void flog(const std::string & entry,bool isEnable=true) {
		if (isEnable) {
			static std::ofstream ofs("D:/log.log", std::ios::app /*| std::ios::app*/);
			ofs << entry << std::endl;
		}
	}

	static void clog(const std::string& entry, bool isEnable = true) {
		if (isEnable) {
			std::cout << entry;
		}
	
	}

	static void printBuf(const string tag, uint8_t* buf, uint16_t bufLen) {
		if (1) {
			printf(tag.c_str());
			for (int i = 0; i < bufLen; i++)
			{
				printf("%02x ", buf[i]);
			}
			printf("\n");
		}
	}
	static void printDifTime(const string tag, DWORD startTime) {
		if (1) {
			printf(tag.c_str());
			DWORD  now_timeStamp = M::TimeInMillisecond();
			printf("%u:", now_timeStamp - startTime);
			printf("\n");

		}
	}



	static void printDifTime(const string tag) {
		static DWORD t1 = timeGetTime();
		if (1) {
			printf(tag.c_str());
			DWORD  now_timeStamp = timeGetTime();;
			printf("  %ld:", now_timeStamp - t1);
			printf("\n");
		}
		t1 = timeGetTime();
	}



	static void printDifTimeToFile(const string tag) {
		static DWORD t1 = timeGetTime();
		DWORD  now_timeStamp = timeGetTime();;
		if (1) {
			char s[64];
			sprintf(s,"=> %ld", now_timeStamp - t1);
			flog(s);
		}
		t1 = timeGetTime();
	}

	static void printToggleTimer(const string tag, uint16_t GPIO_Pin) {
		static DWORD t1[16] = { 0 };
		if (1) {
			printf(tag.c_str());
			printf("%u\n", M::TimeInMillisecond() - t1[GPIO_Pin]);
			t1[GPIO_Pin] = M::TimeInMillisecond();
		}
	}

	static int Str2Int(const char* str)
	{
		int len = strlen(str);
		int t = 0;
		int num = 0;
		for (int i = 0; i < len; i++)
		{
			while (!(str[i] >= 0x30 && str[i] <= 0x39))
			{
				i++;
			}
			while (str[i] >= 0x30 && str[i] <= 0x39)
			{
				t = str[i] - 0x30;
				num = num * 10 + t;
				i++;
			}
		}
		return num;
	}


	static string Int2HexStr(DWORD v)
	{
		stringstream ioss;
		string s_temp; 
		ioss <<"0X" << setiosflags(ios::uppercase) << hex << v;
		ioss >> s_temp;
		return s_temp;

	}


	static string bytes2HexStr(uint8_t* arr, size_t len) {
		ostringstream oss;
		oss << hex << setfill('0');
		for (size_t i = 0; i < len; i++) {
			oss << setw(2) << static_cast<int>(arr[i]) << " ";
		}
		return oss.str();
	}



	static void hexStr2Bytes(const std::string& hexArrStr, BYTE* outBytes, WORD* outLen)
	{
		int bytelen = (hexArrStr.length() + 1) / 3;
		std::string strByte;
		unsigned int n;
		*outLen = bytelen;
		for (int i = 0; i < bytelen; i++)
		{
			strByte = hexArrStr.substr(i * 3, 2);
			sscanf_s(strByte.c_str(), "%x", &n);
			outBytes[i] = n;
		}
	}



	static DWORD TimeInMillisecond(void) {

		return(timeGetTime());
	}

	static string GetTimeStr()
	{
		time_t tNowTime;
		time(&tNowTime);
		tm* tLocalTime = localtime(&tNowTime);
		//"2011-07-18 23:03:01 ";
		string strFormat = "%H:%M:%S  ";
		char strDateTime[30] = { '\0' };
		strftime(strDateTime, 30, strFormat.c_str(), tLocalTime);
		string strRes = strDateTime;
		return strRes;
	}

};

