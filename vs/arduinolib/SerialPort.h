#ifndef SERIALPORT_H_
#define SERIALPORT_H_


#pragma comment(lib,"Winmm.lib")
#include <stdint.h>
#include <Windows.h>



class CSerialPort
{
protected:
	HANDLE  m_hComm;
public:
	CSerialPort(void);
	~CSerialPort(void);

public:
    uint8_t    m_buffer[4096];
	bool Open(uint32_t portNo /*= 1*/, uint32_t baud=115200 /*= CBR_9600*/, char parity = 'N' /*= 'N'*/,
		uint32_t databits = 8 /*= 8*/, uint32_t stopsbits = 1 /*= 1*/, DWORD dwCommEvents = EV_RXCHAR /*= EV_RXCHAR*/);
	virtual DWORD  Send(BYTE* buf, DWORD bufSize);
	virtual DWORD  Receive(BYTE* buf, DWORD bufSize);
	uint32_t GetBytesInCOM();
    int SetRts(int enable);
    int SetCts(int enable);
    bool ReadChar(char& cRecved);
	BOOL IsSendingCompleted(void);
	virtual	void Run(void);
	void Close();

};

#endif 