#define  _CRT_SECURE_NO_WARNINGS
#ifndef _ARDUINO_H
#define _ARDUINO_H
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include "HardwareSerial.h"

#define config_enable_uart 0
#define IS_VS_ENV   1
#define OUTPUT 0
#define INPUT_PULLUP 0
#define LOW               0x0
#define HIGH              0x1

#ifdef __cplusplus 
extern HardwareSerial Serial;
extern HardwareSerial Serial1;
extern HardwareSerial Serial2;

extern "C"
{
#endif
	void delay(uint32_t);
	unsigned long millis();
	void Main();
#ifdef  __cplusplus
}
#endif




#endif
