
#ifndef _HardwareSerial_H
#define _HardwareSerial_H

#include "SerialPort.h"
#include "stdint.h"

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2


void pinMode(uint8_t pin, uint8_t mode);
void digitalWrite(uint8_t pin, uint8_t val);
int digitalRead(uint8_t pin);

class HardwareSerial {
public:
    int m_port;
    CSerialPort* cSerialPort;
    HardwareSerial(int port);
    int available();
    void begin(unsigned long baud);
    size_t write(uint8_t);
    size_t write(const uint8_t *buffer, size_t size);
    size_t readBytes(uint8_t *buffer, size_t length);
    size_t readBytes(char* buffer, size_t length);
    size_t printf(const char * format, ...);
    size_t print(long);
    size_t print(char);
    size_t print(unsigned char, int = DEC);
    size_t print(int, int = DEC);
};


#endif
